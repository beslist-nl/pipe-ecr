#!/usr/bin/env bash

# Mandatory globals
AWS_KEY=${AWS_KEY:? 'AWS ACCESS KEY is missing'}
AWS_SECRET=${AWS_SECRET:? 'AWS SECRET is missing'}
ECR_REGISTRY=${ECR_REGISTRY:? 'Docker registry is missing'}
IMAGE=${IMAGE:? 'Image name is missing'}
TAG=${TAG:? 'tag is missing'}

#optional globals
DOCKERFILE_NAME=${DOCKERFILE_NAME:= 'Dockerfile'}
AWS_REGION=${AWS_REGION:= 'eu-central-1'}

source "/common.sh"

IMAGE_NAME=${ECR_REGISTRY}/${IMAGE}:${TAG}

info  'AWS ECR login'
pip3 install awscli
aws configure set aws_access_key_id "$AWS_KEY"
aws configure set aws_secret_access_key "$AWS_SECRET"
aws configure set default.region "$AWS_REGION"
aws configure set region "$AWS_REGION"
eval $(aws ecr get-login --no-include-email | sed 's;https://;;g')

info 'Start building image'
docker build -f ./${DOCKERFILE_NAME} -t ${IMAGE_NAME} .

info 'Pushing image to registry'
docker push ${IMAGE_NAME}

info 'Storing image name'
echo ${IMAGE_NAME} >> ${BITBUCKET_PIPE_STORAGE_DIR}/image_name

success 'Pipe finished'
